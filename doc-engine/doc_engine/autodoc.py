import datetime
import os

from doc_engine.defaults import Templates


class AutoDoc:

    @staticmethod
    def as_template(dst_dir, file_info, template_env, template_data):
        outfiles = []
        template = template_env.get_template(Templates.autodoc)

        rst_file_name = f'{file_info["name"]}{os.path.extsep}rst'
        file_path_dst = os.path.join(dst_dir, rst_file_name)
        with open(file_path_dst, 'w') as outfile:
            outfile.write(template.render(
                name=file_info['name'],
                page_title=file_info.get('title', file_info['name']),
                description=file_info.get('description'),
                programs=file_info.get('programs', []),
                modules=file_info.get('modules', []),
                imports=file_info.get('imports', []),
                file_type=file_info['type'],
                date=datetime.datetime.now(),
                toc=file_info.toc,
                add_toc=file_info.add_toc,
                add_indexes=file_info.add_indexes,
                version_info=file_info['version_info'],
                **template_data))
        outfiles.append(file_path_dst)
        return outfiles
