import os


class Templates:

    auto_toc = 'auto_toc.j2'
    openapi = 'openapi_template.rst.j2'
    raw_file = 'raw_file_wrapper.rst.j2'
    autodoc = 'autodoc_template.rst.j2'
    inline = 'inline_document.rst.j2'


template_dir = (os.path.dirname(__file__)
                + '/../../source/_templates')
