import os
import shutil

from git import Repo


def git_clone(component_uri, dest_dir, version='master', use_cached=False):
    comp_meta = {}
    if use_cached and os.path.exists(dest_dir):
        repo = Repo(dest_dir)
        comp_meta['version_info'] = {'commit': repo.head.commit.hexsha}

        comp_meta['source_dir'] = dest_dir
    else:
        if os.path.isdir(dest_dir):
            shutil.rmtree(dest_dir)
        repo = Repo.clone_from(component_uri,
                               dest_dir,
                               branch=version)

        comp_meta['version_info'] = {'commit': repo.head.commit.hexsha}
        comp_meta['source_dir'] = repo.working_dir if repo else None

    return repo, comp_meta
