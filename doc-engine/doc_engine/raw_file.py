import datetime
import glob
import os
import shutil

import jinja2


class RawFile:

    @staticmethod
    def wrapped(src_dir, dst_dir, file_info,
                template_name, template_env, template_data):
        """
        Renders raw files wrapped by a template and copies the raw file
        into location.
        """
        text_files = []

        for file_path in glob.iglob(os.path.join(src_dir, file_info.path)):
            template = template_env.get_template(template_name)

            rst_file_name = (os.path.splitext(file_info.name)[0]
                             + os.path.extsep
                             + 'rst')
            raw_file_name = os.path.basename(file_path)
            file_path_dst = os.path.join(dst_dir, rst_file_name)
            raw_file_path_dst = os.path.join(dst_dir, f'{raw_file_name}')
            template_data['path'] = raw_file_name

            # Write templated wrapper
            with open(file_path_dst, 'w') as outfile:
                outfile.write(template.render(
                    page_title=file_info.get('title', file_info.name),
                    date=datetime.datetime.now(),
                    version_info=file_info.version_info,
                    toc=file_info.toc,
                    add_toc=file_info.add_toc,
                    add_indexes=file_info.add_indexes,
                    **template_data))

            # Copy raw file into place
            RawFile.copy(file_path, raw_file_path_dst, file_info)

        try:
            text_files.append(file_path_dst)
            text_files.append(raw_file_path_dst)
        except Exception:
            pass

        return text_files

    @staticmethod
    def copy(dst_dir, file_info):
        """
        Copies a raw file into location.
        """
        copied_files = []
        src_path = file_info.src_paths[-1]
        for file_path in glob.iglob(src_path):
            file_path_dst = os.path.join(dst_dir, os.path.basename(file_path))

            if not os.path.exists(file_path):
                raise FileNotFoundError(f'{file_path} does not exist')

            shutil.copyfile(file_path, file_path_dst)

            copied_files.append(os.path.join(dst_dir, file_path_dst))

        return copied_files

    @staticmethod
    def is_template(dst_dir, file_info, template_data):
        src_path = file_info.src_paths[-1]
        loader = jinja2.FileSystemLoader(searchpath=os.path.dirname(src_path))
        template_env = jinja2.Environment(loader=loader)

        template = template_env.get_template(
            os.path.basename(src_path)
        )

        file_path_dst = os.path.join(dst_dir, file_info.path)
        with open(file_path_dst, 'w') as outfile:
            outfile.write(template.render(date=datetime.datetime.now(),
                                          version_info=file_info.version_info,
                                          **template_data))
        return [file_path_dst]
