import glob
import os
import pypandoc

from doc_engine.defaults import Templates


class RSTHelpers:

    @staticmethod
    def add_toc(file_path, file_info, template_env, template_data):
        """
        Appends a table of contents to the end of an rst file.
        """

        file_info['toc'].remove(file_info.name)
        with open(file_path, 'a') as outfile:
            template = template_env.get_template(Templates.auto_toc)
            outfile.write(template.render(component=file_info.component,
                                          toc=file_info.toc,
                                          add_indexes=file_info.add_indexes,
                                          add_toc=file_info.add_toc,
                                          **template_data))

    @staticmethod
    def convert_md(dst_dir, file_info, template_env, template_data):
        md_files = []
        src_path = file_info.src_paths[-1]

        for file_path in glob.iglob(src_path):
            name = os.path.splitext(file_info['name'])[0]
            file_path_dst = os.path.join(dst_dir, f'{name}.rst')

            pypandoc.convert_file(file_path,
                                  'rst',
                                  format='gfm',
                                  outputfile=file_path_dst)
            if file_info.add_toc or file_info.add_indexes:
                RSTHelpers.add_toc(file_path_dst, file_info, template_env,
                                   template_data)

            md_files.append(file_path_dst)

        return md_files
