import argparse
import shutil
import os

from attrdict import AttrDict
import requests
import yaml

import urllib.parse as url_parse

import doc_engine.defaults as defaults
from doc_engine.git import git_clone
from doc_engine.components import build_components
from doc_engine.util import resolve_path


def build_args():
    """CLI args object unparsed"""
    args = argparse.ArgumentParser('piperci release fetcher')

    release_group = args.add_mutually_exclusive_group(required=True)

    release_group.add_argument(
        '--release-file-uri',
        help='The location of the release config file '
             'to load componants from.')

    release_group.add_argument('--docs-yaml-uri',
                               help='Use a specific docs.yml at uri or file path')

    args.add_argument('--release',
                      default='altstadt',
                      help='The current release to build against: %(default)s')

    args.add_argument('--build-dir',
                      default='build',
                      help='The build staging directory or dest to copy README files'
                           'default: %(default)s')

    args.add_argument('--cache-dir',
                      default=os.path.join('cache', 'compenents'),
                      help='The cache directory to save compenents in'
                           'default: %(default)s')

    args.add_argument('--use-cached',
                      action='store_true',
                      help='Don\'t download components if they exist in cache-dir')

    args.add_argument('--component-requirements',
                      action='store_true',
                      help='Generate a requirements file for components in build-dir')

    args.add_argument('--template-dir',
                      default=defaults.template_dir,
                      help='App templates directory, dir for Templates in '
                           'defaults.Templates')

    args.add_argument('--values-file',
                      default=None,
                      help='Override values in user templates with these')

    return args


def copy_local_dir(src, dst, use_cached=False):
    src = resolve_path(src)
    dst = resolve_path(dst)

    if use_cached:
        if not os.path.exists(dst):
            raise FileNotFoundError(f'{dst} does not exist')

        return {'version_info': 'local',
                'source_dir': dst}
    else:
        if os.path.exists(dst):
            shutil.rmtree(dst)

        if os.path.isdir(src):
            shutil.copytree(src, dst, copy_function=shutil.copy)
        else:
            raise FileNotFoundError(f'{src} does not exist')

    return {'version_info': 'local',
            'source_dir': dst}


def fetch_components(components, dst_path, use_cached=False):
    """Clone target source repositories to the src_path.
    :components list: A list of dictionaries with the components section of a release
    :dst_path str: A string path where the repositories should be cloned
    :return: The list of components
    """

    for component in components:
        repo_dir = os.path.join(dst_path, component['name'])

        if dst_path not in repo_dir:
            raise RuntimeError(f'{repo_dir} is not under {dst_path}')

        comp_uri = url_parse.urlparse(component['uri'])
        if os.path.splitext(comp_uri.path)[1] == '.git':
            repo, comp_meta = git_clone(component['uri'],
                                        repo_dir,
                                        component['version'],
                                        use_cached=use_cached)
            component.update(comp_meta)
        else:
            comp_meta = copy_local_dir(component['uri'], repo_dir, use_cached)
            component.update(comp_meta)

    return components


def get_docs_yml(docs_uri):
    if isinstance(docs_uri, str):
        docs_uri = url_parse.urlparse(docs_uri)

    if docs_uri.scheme and docs_uri.netloc:
        resp = requests.get(docs_uri.geturl())
        resp.raise_for_status()
        text = resp.text
    else:
        with open(resolve_path(docs_uri.geturl()), 'r') as docs_file:
            text = docs_file.read()

    return yaml.safe_load(text)


def get_docs_from_release(uri, release):
    """Fetch the release file from uri.
    :uri str: A file path or web uri to a piperci release meta yaml file
    :return: Dictionary loaded releases file
    """

    base_def_uri = url_parse.urlparse(uri)

    resp = requests.get(base_def_uri.geturl())
    resp.raise_for_status()

    release_base = yaml.safe_load(resp.text)

    doc_path = release_base['releases'][release]['components']['docs']['config']

    docs_uri = base_def_uri._replace(path=os.path.join(
        os.path.dirname(base_def_uri.path), doc_path))

    return get_docs_yml(docs_uri)


def main():

    args = build_args()
    opts = args.parse_args()

    src_path = resolve_path(opts.cache_dir)
    dst_path = resolve_path(opts.build_dir)

    if opts.docs_yaml_uri:
        docs_def = get_docs_yml(opts.docs_yaml_uri)
    elif opts.release_file_uri:
        docs_def = get_docs_from_release(opts.release_file_uri, opts.release)
    else:
        print('Define either a release file uri or docs.yml uri')
        raise SystemExit(1)

    components = fetch_components(docs_def['docs'], src_path,
                                  use_cached=opts.use_cached)

    if opts.values_file:
        with open(resolve_path(opts.values_file), 'r') as values_file:
            values = AttrDict(yaml.safe_load(values_file)['values'])
    else:
        values = AttrDict({})

    clean_list, doc_files, requirements = build_components(components,
                                                           dst_path,
                                                           opts.template_dir,
                                                           values=values)

    if opts.component_requirements:
        with open(os.path.join(dst_path, 'component-requirements.txt'),
                  'w') as comp_file:
            comp_file.writelines(req + '\n' for req in set(requirements))

    nl = '\n'
    print('Created:\n', f'{nl.join(clean_list)}')
    print('Copied:\n', f'{nl.join(doc_files)}')

    with open('.cleanup', 'a') as cleanups:

        cleanups.writelines(line + '\n' for line in clean_list)
        cleanups.writelines(line + '\n' for line in doc_files)


if __name__ == '__main__':
    main()
