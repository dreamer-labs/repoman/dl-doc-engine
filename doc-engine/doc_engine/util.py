import os


def resolve_path(path):
    return os.path.realpath(os.path.expandvars(os.path.expanduser(path)))
